<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->unique()->name,
            'quantity' => $this->faker->numberBetween(1,30),
            'price' => $this->faker->randomFloat('2',0,2),
            'public_at' => $this->faker->dateTimeThisMonth()->format('Y-m-d H:i:s')
            // 'name' => 'book',
            // 'quantity' => 20,
            // 'price' => 3.2,

        ];
    }
}
